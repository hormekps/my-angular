import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  myForm: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {

    this.myForm = this.fb.group({  name: [null, [Validators.required, Validators.maxLength(10)]],
      roomNumbers: [null, []],
      pool: [null, []],
    });

  }
sendForm() {
    if (this.myForm.valid)
    {
      console.log(this.myForm.value);
    } else {
      alert('formulaire pas valable');
    }



}

}
