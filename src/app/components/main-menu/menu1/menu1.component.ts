import {Component, Input, OnInit} from '@angular/core';
import {MenuModel} from '../../../models/menu.model';
import {MenuService} from '../../../services/menu.service';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {


  @Input() age: number;
  routes: MenuModel [];


  name = 'hormeli';


  constructor(
    private menuService: MenuService
  ) {

  }

  ngOnInit(): void {
this.routes = this.menuService.allRoute;
  }
  sayHello(){
    return {name: 'joe', age: 25};
  }
}

