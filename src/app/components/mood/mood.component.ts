import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.scss']
})
export class MoodComponent implements OnInit {

  @Output() handleMoodChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }
 iAmOk(){

this.handleMoodChange.emit('ok');
 }
 iAmNotOk(){

this.handleMoodChange.emit('not ok');
 }
 iAmNeutral(){

this.handleMoodChange.emit('neutral');
 }
}
