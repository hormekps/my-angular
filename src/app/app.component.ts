import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuModel} from './models/menu.model';
import {MenuService} from './services/menu.service';
import {HotelService} from './services/hotel.service';
import {takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'promo3Angular';
  message: string;
  destroy$: Subject<boolean> = new Subject();

  allRoute: MenuModel [] = this.menuService.allRoute;
  age = 53;

  constructor(
    private menuService: MenuService,
    private hotelService: HotelService,
  ) {
  }

  manageMoodChange(message: string){
this.message = message;
  }

  ngOnInit(): void {
    this.hotelService.getAllHotel$()
      .pipe(
        takeUntil(this.destroy$),
        tap(x => console.log(x)),
      )
      .subscribe();


  }
  ngOnDestroy(): void {
this.destroy$.next(true);
this.destroy$.complete();
  }
creatHotel(): void{
    this.hotelService.creatNewHotel$({
      name: 'hotel de Massiah ',
      roomNumbers: 7,
      pool: false
    })
      .subscribe();
}
deleteHotel(): void {
    this.hotelService.deleteHotelById$('9gGLiJAkPzE7VXTqpHoz')
      .subscribe();
}
putHotel(): void {
    this.hotelService.putHotelById$( 'BlvBfhac1CzHwLgEg4c3', {
      name: 'hotel elisha ',
      roomNumbers: 33,
      pool: true } , )
      .subscribe();
}

updateHotel(): void {
    this.hotelService.updateHotelById$('BlvBfhac1CzHwLgEg4c3', {
      name: 'hotel ma belle ',
      roomNumbers: 33,
      pool: true } , )
      .subscribe();
}
}
