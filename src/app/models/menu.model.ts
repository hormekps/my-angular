export interface MenuModel {
  url: string;
  name: string;
  minimunAge: number;
}
