import { Injectable } from '@angular/core';
import {MenuModel} from '../models/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  // @ts-ignore
  allRoute: MenuModel [] = [
    {url: 'home', name : 'accueil', minimunAge : 0},
    {url: 'à-propos', name: 'à propos', minimunAge : 30},
    {url: 'form', name: 'formulaire', minimunAge : 30}
  ];


  constructor() { }
}
