import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HotelModel} from '../models/hotel.model';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

root: string = environment.api;

  constructor(
    private httpClient: HttpClient,
  ) { }

  getHotelById$(hotelId: string): Observable<HotelModel> {
    return this.httpClient.get(this.root + hotelId) as Observable<HotelModel> ;
  }

  getAllHotel$(): Observable<HotelModel[]> {
    return this.httpClient.get(this.root + 'hostels') as Observable<HotelModel[]>;
  }

  creatNewHotel$(hotel: HotelModel): Observable<HotelModel>{
    return (this.httpClient.post(this.root + 'hostels', hotel) as Observable<HotelModel>)
      .pipe
      (tap
      (x =>  console.log(x))
  );

  }
  deleteHotelById$(hotelId: string): Observable<HotelModel> {
    return (this.httpClient.delete(this.root + 'hostels/' + hotelId) as Observable<HotelModel>)
      .pipe
      (tap
      (x => console.log(x)));
  }

  putHotelById$(hostelId: string, hotel: HotelModel ): Observable<HotelModel> {
    return (this.httpClient.put(this.root + 'hostels/' +  hostelId, hotel ) as Observable<HotelModel>)
      .pipe
      (tap
      (x => console.log(x)));
  }
   updateHotelById$(hostelId: string, hotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient.patch(this.root + 'hostels/' + hostelId, hotel) as Observable<HotelModel>)
      .pipe
      (tap
      (x => console.log(x)));
   }
}
